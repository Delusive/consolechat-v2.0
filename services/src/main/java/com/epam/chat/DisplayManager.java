package com.epam.chat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class DisplayManager {
    private final Logger LOGGER = LogManager.getLogger();

    public void print(String str) {
        LOGGER.info(str);
    }

    public void print(Throwable throwable) {
        LOGGER.error(throwable);
    }
}
