package com.epam.chat;

import com.epam.chat.exception.PropertiesException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class PropertiesManager {
    private final Properties PROPERTIES = new Properties();
    private final InputStream INPUT_STREAM;

    public PropertiesManager(String fileName, DisplayManager displayManager) throws PropertiesException {
        INPUT_STREAM = getClass().getResourceAsStream(fileName);
        if (INPUT_STREAM == null) throw new PropertiesException("File \"" + fileName + "\" not found!");
        try {
            PROPERTIES.load(INPUT_STREAM);
        } catch (IOException ex) {
            displayManager.print(ex);
        }
    }

    public int getInt(String key) throws PropertiesException {
        return Integer.parseInt(getString(key));
    }

    public String getString(String key) throws PropertiesException {
        baseCheck(key);
        return PROPERTIES.getProperty(key);
    }

    private void baseCheck(String key) throws PropertiesException {
        if (key == null) throw new PropertiesException("Parameter mustn't be null!");
        if (INPUT_STREAM == null) throw new PropertiesException("Properties file not loaded!");
    }
}
