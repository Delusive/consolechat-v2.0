package com.epam.chat.exception;

class ConsoleChatException extends RuntimeException {
    ConsoleChatException(String s) {
        super(s);
    }
}
