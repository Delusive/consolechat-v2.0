package com.epam.chat;

class Constants {
    private static final DisplayManager DISPLAY_MANAGER = new DisplayManager();
    private static final PropertiesManager PROPERTIES_MANAGER = new PropertiesManager("/client.properties", DISPLAY_MANAGER);

    static final String HOST = PROPERTIES_MANAGER.getString("server.host");
    static final int PORT = PROPERTIES_MANAGER.getInt("server.port");
}
