package com.epam.chat;

import java.io.IOException;
import java.util.Scanner;

class Client {

    public static void main(String[] args) {
        DisplayManager displayManager = new DisplayManager();
        ConnectionManager connectionManager = new ConnectionManager(Constants.HOST, Constants.PORT, displayManager);
        Scanner scanner = new Scanner(System.in);
        try (ConnectionManager manager = connectionManager) {
            manager.start();
            while (true) {
                String msg = scanner.nextLine();
                manager.sendMessageToServer(msg);
            }
        } catch (IOException e) {
            displayManager.print(e);
        }
    }

}
