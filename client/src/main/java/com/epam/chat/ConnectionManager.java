package com.epam.chat;

import java.io.*;
import java.net.Socket;

class ConnectionManager extends Thread implements Closeable {
    private final DisplayManager DISPLAY_MANAGER;
    private BufferedReader bufferedReader;
    private PrintWriter printWriter;

    ConnectionManager(String host, int port, DisplayManager displayManager) {
        try {
            DISPLAY_MANAGER = displayManager;
            Socket socket = new Socket(host, port);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            printWriter = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        try (BufferedReader in = bufferedReader) {
            while (true) {
                String msg = in.readLine();
                DISPLAY_MANAGER.print(msg);
                //Костыль номер один:
                if (msg.equals("Disconnected.")) {
                    System.exit(1);
                }
            }
        } catch (IOException e) {
            DISPLAY_MANAGER.print("Server closed :(");
            System.exit(2); // - Костыль номер два
        }
    }

    void sendMessageToServer(String message) {
        printWriter.println(message);
    }

    @Override
    public void close() throws IOException {
        bufferedReader.close();
        printWriter.close();
    }
}
