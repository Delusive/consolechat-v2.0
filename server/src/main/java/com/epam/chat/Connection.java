package com.epam.chat;

import java.io.*;
import java.net.Socket;
import java.util.Iterator;

class Connection extends Thread implements Closeable {
    private boolean autoRemoveFromConnectionList = true;
    private final Socket socket;
    private final DisplayManager DISPLAY_MANAGER;
    private final ConnectionManager CONNECTION_MANAGER;
    private final MessageHistory MESSAGE_HISTORY;
    private BufferedReader in;
    private PrintWriter out;
    private String username = "";

    Connection(Socket accepted, DisplayManager displayManager, ConnectionManager connectionManager) {
        socket = accepted;
        DISPLAY_MANAGER = displayManager;
        CONNECTION_MANAGER = connectionManager;
        MESSAGE_HISTORY = connectionManager.getMESSAGE_HISTORY();
        try {
            in = new BufferedReader(new InputStreamReader(accepted.getInputStream()));
            out = new PrintWriter(accepted.getOutputStream(), true);
        } catch (IOException e) {
            DISPLAY_MANAGER.print(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        try {
            String temp;
            //Запрашиваем ник:
            out.println("Enter ur nickname:");
            while (true) {
                temp = in.readLine();
                if (temp == null) continue;
                if (isUsernameSuitable(temp)) break;
                out.println("Invalid username. Try again.");
                DISPLAY_MANAGER.print("User tried to use invalid username, rejected.");
            }
            this.username = temp;
            DISPLAY_MANAGER.print(username + " joined to the chat");
            CONNECTION_MANAGER.broadcastMessage(username + " connected.");
            //Отправка истории:
            synchronized (MESSAGE_HISTORY) {
                Iterator histIterator = CONNECTION_MANAGER.getMESSAGE_HISTORY().iterator();
                if (histIterator.hasNext()) out.println("Message history:");
                while (histIterator.hasNext()) out.println(histIterator.next());
            }
            while (true) {
                String msg = in.readLine();
                DISPLAY_MANAGER.print(username + " sent message: " + msg);
                String broadcastMsg = String.format("<%s> %s", username, msg);
                CONNECTION_MANAGER.broadcastMessage(broadcastMsg);
                CONNECTION_MANAGER.getMESSAGE_HISTORY().addMessage(broadcastMsg);
            }
        } catch (IOException e) {
            DISPLAY_MANAGER.print(username + " disconnected.");
            if (autoRemoveFromConnectionList) {
                CONNECTION_MANAGER.getCONNECTIONS().remove(this);
                CONNECTION_MANAGER.broadcastMessage(username + " disconnected.");
            }
        }
    }

    //Отправка сообщения этому пользователю:
    void sendMessage(String str) {
        out.println(str);
    }

    //Проверка ника на то, является ли он подходящим:
    private boolean isUsernameSuitable(String username) {
        if (!username.matches(Constants.REGEXP_USERNAME)) return false;
        for (Connection connection : CONNECTION_MANAGER.getCONNECTIONS()) {
            if (connection.getUsername().equalsIgnoreCase(username)) return false;
        }
        return true;
    }

    String getUsername() {
        return username;
    }

    void setAutoRemoveFromConnectionList(boolean autoRemoveFromConnectionList) {
        this.autoRemoveFromConnectionList = autoRemoveFromConnectionList;
    }

    @Override
    public void close() throws IOException {
        sendMessage("Disconnected.");
        socket.close();
    }
}
