package com.epam.chat;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class MessageHistory implements Serializable {
    private final List<String> HISTORY = new ArrayList<>();
    private final String FILE_NAME;
    private final int storageSize;

    MessageHistory(int storageSize, String fileName) {
        this.storageSize = storageSize;
        FILE_NAME = fileName;
    }

    synchronized void addMessage(String str) {
        if (HISTORY.size() >= storageSize) {
            removeFirst(HISTORY.size() - storageSize); //Удаление лишних элементов
            HISTORY.remove(0); //И еще один удаляем :р
        }
        HISTORY.add(str);
    }

    private synchronized void removeFirst(int count) {
        if (count > 0) HISTORY.subList(0, count).clear();
    }

    Iterator iterator() {
        return HISTORY.iterator();
    }

    void serialize() throws RuntimeException {
        try {
            File saveFile = new File(FILE_NAME);
            saveFile.createNewFile();

            FileOutputStream outputStream = new FileOutputStream(saveFile);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(this);
            objectOutputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static MessageHistory deserialize(String fileName) throws RuntimeException {
        try {
            FileInputStream inputStream = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            return (MessageHistory) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
