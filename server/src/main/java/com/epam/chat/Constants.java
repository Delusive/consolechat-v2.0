package com.epam.chat;

class Constants {
    private static final DisplayManager displayManager = new DisplayManager();
    private static final PropertiesManager propertiesManager = new PropertiesManager("/server.properties", displayManager);

    static final int PORT = propertiesManager.getInt("server.port");
    static final String REGEXP_USERNAME = propertiesManager.getString("username.regexp");
    static final String HISTORY_FILE_NAME = propertiesManager.getString("history.file.name");
    static final int HISTORY_SIZE = propertiesManager.getInt("history.size");
}
