package com.epam.chat;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

class ConnectionManager extends Thread implements Closeable {
    private final List<Connection> CONNECTIONS = new ArrayList<>();
    private final MessageHistory MESSAGE_HISTORY;// = new MessageHistory(Constants.HISTORY_SIZE);
    private final DisplayManager DISPLAY_MANAGER;
    private final int SERVER_PORT;
    private ServerSocket serverSocket;

    ConnectionManager(DisplayManager displayManager, int serverPort) {
        SERVER_PORT = serverPort;
        DISPLAY_MANAGER = displayManager;
        if(new File(Constants.HISTORY_FILE_NAME).exists()) {
            MESSAGE_HISTORY = MessageHistory.deserialize(Constants.HISTORY_FILE_NAME);
        } else {
            MESSAGE_HISTORY = new MessageHistory(Constants.HISTORY_SIZE, Constants.HISTORY_FILE_NAME);
        }
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(SERVER_PORT);
            while (true) {
                Socket accepted = serverSocket.accept();
                Connection client = new Connection(accepted, DISPLAY_MANAGER, this);
                CONNECTIONS.add(client);
                client.start();
                DISPLAY_MANAGER.print("Someone connected.");
            }
        } catch (IOException ex) {
            DISPLAY_MANAGER.print(ex);
        }
        try {
            DISPLAY_MANAGER.print("Saving history...");
            MESSAGE_HISTORY.serialize();
            DISPLAY_MANAGER.print("History saved!");
        } catch (RuntimeException e) {
            DISPLAY_MANAGER.print(e);
        }
    }

    void broadcastMessage(String str) {
        CONNECTIONS.forEach(connection -> {
            if (!connection.getUsername().equals("")) connection.sendMessage(str);
        });
    }

    List<Connection> getCONNECTIONS() {
        return CONNECTIONS;
    }

    MessageHistory getMESSAGE_HISTORY() {
        return MESSAGE_HISTORY;
    }

    @Override
    public void close() throws IOException {
        for (Connection connection : CONNECTIONS) {
            connection.setAutoRemoveFromConnectionList(false);
            connection.close();
        }
        CONNECTIONS.clear();
        serverSocket.close();
    }
}
