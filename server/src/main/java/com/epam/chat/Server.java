package com.epam.chat;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

class Server {

    public static void main(String[] args) {
        DisplayManager displayManager = new DisplayManager();
        ConnectionManager connectionManager = new ConnectionManager(displayManager, Constants.PORT);
        connectionManager.start();
        displayManager.print("Server started.");
        Scanner in = new Scanner(System.in);

        outside:
        while (in.hasNext()) {
            String cmd = in.next();
            switch (cmd) {
                case "stop":
                    try {
                        connectionManager.close();
                    } catch (IOException e) {
                        displayManager.print(e);
                    }
                    break outside;
                case "list":
                    List<Connection> connections = connectionManager.getCONNECTIONS();
                    boolean isSomeoneConnected = false;
                    for (Connection c : connections) {
                        if (!c.getUsername().equals("")) {
                            isSomeoneConnected = true;
                            break;
                        }
                    }
                    StringBuilder answer = new StringBuilder();
                    if (!isSomeoneConnected) {
                        answer.append("No users connected :p");
                    } else {
                        answer.append("Online users: ");
                        connections.forEach(connection -> {
                            if (!connection.getUsername().equals("")) {
                                answer.append(connection.getUsername()).append(", ");
                            }
                        });
                        int index = answer.lastIndexOf(",");
                        answer.delete(index, index + 2).append('.');
                    }
                    displayManager.print(answer.toString());
                    break;
                default:
                    displayManager.print("Unknown command. Available commands: stop, list.");
                    break;
            }
        }
        displayManager.print("Server closed.");
    }

}
